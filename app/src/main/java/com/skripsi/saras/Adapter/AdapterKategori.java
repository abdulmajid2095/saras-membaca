package com.skripsi.saras.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.skripsi.saras.Model.DataKategori;
import com.skripsi.saras.R;
import com.skripsi.saras.Service.OnLoadMoreListener;
import com.skripsi.saras.Start.Detail_Kategori;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;


public class AdapterKategori extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    List<DataKategori> myArray;
    RecyclerView mRecyclerView;
    private boolean isLoading;
    private int visibleThreshold = 1;
    private int lastVisibleItem, totalItemCount;
    private OnLoadMoreListener mOnLoadMoreListener;

    Context c;
    public AdapterKategori(Context c, RecyclerView mRecyclerView, List<DataKategori> myArray) {
        this.mRecyclerView = mRecyclerView;
        this.myArray = myArray;
        this.c = c;

        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView.getLayoutManager();
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (mOnLoadMoreListener != null) {
                        mOnLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }
    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(c).inflate(R.layout.model_kategori, parent, false);
            return new AdapterKategori.UserViewHolder(view);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(c).inflate(R.layout.progressbar, parent, false);
            return new AdapterKategori.LoadingViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof AdapterKategori.UserViewHolder) {
         final DataKategori dataKategori = myArray.get(position);
            AdapterKategori.UserViewHolder userViewHolder = (AdapterKategori.UserViewHolder) holder;

            userViewHolder.textJudul.setText(""+dataKategori.getjudul());
            userViewHolder.textDeskripsi.setText(""+dataKategori.getDeskripsi());

            String url = ""+dataKategori.getGambar();
            if(url.equals(""))
            {
                url = "-";
            }
            Picasso.with(c)
                    .load("https://tumbas.net/membaca/image/" + url)
                    .noPlaceholder()
                    .resize(400, 400)
                    .centerCrop()
                    .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                    .into(userViewHolder.gambar);

            userViewHolder.parent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(c, Detail_Kategori.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("idKategori",""+dataKategori.getidKategori());
                    intent.putExtra("judul",""+dataKategori.getjudul());
                    intent.putExtra("bacaan",""+dataKategori.getBacaan());
                    intent.putExtra("gambar",""+dataKategori.getGambar());
                    intent.putExtra("jumlahKata",""+dataKategori.getJumlahKata());
                    c.startActivity(intent);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return myArray == null ? 0 : myArray.size();
    }

    @Override
    public int getItemViewType(int position) {
        return myArray.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }
    public void setLoaded() {
        isLoading = false;
    }

    public class UserViewHolder extends RecyclerView.ViewHolder {
        LinearLayout parent;
        TextView textJudul, textDeskripsi;
        ImageView gambar;

        public UserViewHolder(View view) {
            super(view);
            c = itemView.getContext();

            parent = view.findViewById(R.id.parent);
            textJudul = view.findViewById(R.id.judul);
            textDeskripsi = view.findViewById(R.id.deskripsi);
            gambar = view.findViewById(R.id.gambar);
        }
    }

    static class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar1);
        }
    }
}
