package com.skripsi.saras.LocalDatabase;

import com.orm.SugarRecord;

public class DataUser extends SugarRecord {

    public String imei, nama;
    public Boolean showTimer;
    public Boolean soundOn;
    public DataUser() {

    }

    public DataUser(String imei, String nama, Boolean showTimer, Boolean soundOn) {

        this.imei = imei;
        this.nama = nama;
        this.showTimer = showTimer;
        this.soundOn = soundOn;
    }
}