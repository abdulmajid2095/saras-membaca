package com.skripsi.saras;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;

import com.skripsi.saras.LocalDatabase.DataUser;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_main);

        DataUser data = DataUser.findById(DataUser.class,1L);
        if((int)data.count(DataUser.class, "", null) == 0) {
            DataUser datax = new DataUser("", "", true, false);
            datax.save();
        }

        Handler handler =  new Handler();
        Runnable myRunnable = new Runnable() {
            public void run() {
                Intent intent = new Intent(MainActivity.this, MasukkanNama.class);
                startActivity(intent);

            }
        };
        handler.postDelayed(myRunnable,2000);
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }
}
