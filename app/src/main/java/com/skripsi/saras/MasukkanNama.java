package com.skripsi.saras;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.skripsi.saras.LocalDatabase.DataUser;
import com.skripsi.saras.Service.MusicService;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MasukkanNama extends AppCompatActivity {

    @BindView(R.id.editNama) EditText editNama;
    @BindView(R.id.lanjut) LinearLayout lanjut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_masukkan_nama);
        ButterKnife.bind(this);

        DataUser dataUser = DataUser.findById(DataUser.class, 1L);
        if(dataUser.soundOn)
        {
            startService(new Intent(this, MusicService.class));
        }

        lanjut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String cek = ""+editNama.getText().toString();
                if(cek.isEmpty())
                {
                    editNama.setError("Masukkan nama kamu disini");
                }
                else
                {
                    editNama.setError(null);
                    DataUser dataUser = DataUser.findById(DataUser.class,1L);
                    dataUser.nama = ""+cek;
                    dataUser.save();
                    Intent intent = new Intent(MasukkanNama.this, MenuUtama.class);
                    startActivity(intent);
                }
            }
        });

        checkPermission();
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }

    public void checkPermission()
    {
        int permission5 = ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_PHONE_STATE);

        if (permission5 != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(MasukkanNama.this,
                    new String[]{Manifest.permission.READ_PHONE_STATE},
                    345);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {

        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
        {
            checkPermission();
        }
        else
        {
            /*location 341, 342
            phone 343, 344, 345
            storage 347, 348*/
            if(requestCode == 345)
            {
                dialogPermissionPhone();
            }

        }
    }

    public void dialogPermissionPhone()
    {
        // custom dialog
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_permission_call);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(false);

        Button ok = (Button) dialog.findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                checkPermission();
            }
        });

        dialog.show();
    }

}
