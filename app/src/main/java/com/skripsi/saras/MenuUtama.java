package com.skripsi.saras;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.skripsi.saras.LocalDatabase.DataUser;
import com.skripsi.saras.Service.MusicService;
import com.skripsi.saras.Start.Kategori;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MenuUtama extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.nama) TextView textNama;
    @BindView(R.id.skor) LinearLayout skor;
    @BindView(R.id.start) CardView start;
    @BindView(R.id.settings) CardView settings;
    @BindView(R.id.rule) CardView rule;
    @BindView(R.id.about) CardView about;

    @BindView(R.id.selamat) TextView selamat;
    @BindView(R.id.layScheme) LinearLayout layScheme;
    @BindView(R.id.header) LinearLayout header;
    @BindView(R.id.tb1) LinearLayout tb1;
    @BindView(R.id.tb2) LinearLayout tb2;
    @BindView(R.id.tb3) LinearLayout tb3;
    @BindView(R.id.tb4) LinearLayout tb4;

    String imei = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_menu_utama);

        ButterKnife.bind(this);
        DataUser dataUser = DataUser.findById(DataUser.class,1L);
        textNama.setText("Halo "+dataUser.nama);
        skor.setOnClickListener(this);
        start.setOnClickListener(this);
        settings.setOnClickListener(this);
        rule.setOnClickListener(this);
        about.setOnClickListener(this);

        sendImei();
        setUpTema();
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        dialogkeluar();
    }

    public void dialogkeluar() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        Button ya = dialog.findViewById(R.id.ya);
        ya.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                stopService(new Intent(getApplicationContext(), MusicService.class));
                Intent keluar = new Intent(MenuUtama.this, MainActivity.class);
                startActivity(keluar);
                finish();
                moveTaskToBack(true);

            }
        });

        Button tidak = dialog.findViewById(R.id.tidak);
        tidak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.skor:
                Intent skor = new Intent(MenuUtama.this, Skor.class);
                startActivity(skor);
                break;

            case R.id.start:
                Intent start = new Intent(MenuUtama.this, Kategori.class);
                startActivity(start);
                break;

            case R.id.settings:
                Intent settings = new Intent(MenuUtama.this, Settings.class);
                startActivity(settings);
                break;

            case R.id.rule:
                Intent rule = new Intent(MenuUtama.this, Rule.class);
                startActivity(rule);
                break;

            case R.id.about:
                Intent about = new Intent(MenuUtama.this, About.class);
                startActivity(about);
                break;
        }
    }


    public void sendImei(){

        String imei="";
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            imei = android.provider.Settings.Secure.getString(getApplicationContext().getContentResolver(),
                    android.provider.Settings.Secure.ANDROID_ID);
        }
        else
        {
            TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            imei = telephonyManager.getDeviceId();
        }

            DataUser dataUser = DataUser.findById(DataUser.class,1L);
            dataUser.imei = imei;
            dataUser.save();
            //Toast.makeText(getApplicationContext(),"IMEI : "+device, Toast.LENGTH_LONG).show();

    }


    public void setUpTema()
    {
        Calendar rightNow = Calendar.getInstance();
        //int jam = rightNow.get(Calendar.HOUR_OF_DAY);
        int jam = 20;
        //SORE
        if(jam > 14 && jam < 19)
        {
            selamat.setText("Selamat Sore");
            layScheme.setBackgroundColor(getResources().getColor(R.color.temaSore));
            tb1.setBackgroundColor(getResources().getColor(R.color.tombolSore));
            tb2.setBackgroundColor(getResources().getColor(R.color.tombolSore));
            tb3.setBackgroundColor(getResources().getColor(R.color.tombolSore));
            tb4.setBackgroundColor(getResources().getColor(R.color.tombolSore));
            header.setBackground(getResources().getDrawable(R.drawable.bg_gradient_header_sore));

        }
        //MALAM
        else if(jam > 18 || jam < 4)
        {
            selamat.setText("Selamat Malam");
            layScheme.setBackgroundColor(getResources().getColor(R.color.temaMalam));
            tb1.setBackgroundColor(getResources().getColor(R.color.tombolMalam));
            tb2.setBackgroundColor(getResources().getColor(R.color.tombolMalam));
            tb3.setBackgroundColor(getResources().getColor(R.color.tombolMalam));
            tb4.setBackgroundColor(getResources().getColor(R.color.tombolMalam));
            header.setBackground(getResources().getDrawable(R.drawable.bg_gradient_header_malam));
        }
        //PAGI
        else if(jam > 4 && jam < 11)
        {
            selamat.setText("Selamat Pagi");
            layScheme.setBackgroundColor(getResources().getColor(R.color.temaPagi));
            tb1.setBackgroundColor(getResources().getColor(R.color.tombolPagi));
            tb2.setBackgroundColor(getResources().getColor(R.color.tombolPagi));
            tb3.setBackgroundColor(getResources().getColor(R.color.tombolPagi));
            tb4.setBackgroundColor(getResources().getColor(R.color.tombolPagi));
            header.setBackground(getResources().getDrawable(R.drawable.bg_gradient_header_pagi));
        }
        else
        {
            selamat.setText("Selamat Siang");
            layScheme.setBackgroundColor(getResources().getColor(R.color.temaSiang));
            tb1.setBackgroundColor(getResources().getColor(R.color.tombolSiang));
            tb2.setBackgroundColor(getResources().getColor(R.color.tombolSiang));
            tb3.setBackgroundColor(getResources().getColor(R.color.tombolSiang));
            tb4.setBackgroundColor(getResources().getColor(R.color.tombolSiang));
            header.setBackground(getResources().getDrawable(R.drawable.bg_gradient_header_siang));
        }
    }
}
