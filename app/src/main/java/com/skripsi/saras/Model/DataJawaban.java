package com.skripsi.saras.Model;

import com.google.gson.annotations.SerializedName;

public class DataJawaban {

    @SerializedName("jawaban")
    private String jawaban;

    @SerializedName("dijawab")
    private int dijawab;

    public DataJawaban(String jawaban, int dijawab){
        this.jawaban = jawaban;
        this.dijawab = dijawab;
    }

    public String getJawaban() {
        return jawaban;
    }

    public void setJawaban(String jawaban) {
        this.jawaban = jawaban;
    }

    public int getDijawab() {
        return dijawab;
    }

    public void setDijawab(int dijawab) {
        this.dijawab = dijawab;
    }
}
