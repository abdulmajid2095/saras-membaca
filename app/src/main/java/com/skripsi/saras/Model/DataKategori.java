package com.skripsi.saras.Model;

import com.google.gson.annotations.SerializedName;

public class DataKategori {

    @SerializedName("idKategori")
    private String idKategori;

    @SerializedName("judul")
    private String judul;

    @SerializedName("deskripsi")
    private String deskripsi;

    @SerializedName("bacaan")
    private String bacaan;

    @SerializedName("gambar")
    private String gambar;

    @SerializedName("jumlahKata")
    private String jumlahKata;

    public DataKategori(String idKategori, String judul, String deskripsi, String bacaan, String gambar, String jumlahKata){
        this.idKategori = idKategori;
        this.judul = judul;
        this.deskripsi = deskripsi;
        this.bacaan = bacaan;
        this.gambar = gambar;
        this.jumlahKata = jumlahKata;
    }

    public String getidKategori() {
        return idKategori;
    }

    public String getjudul() {
        return judul;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public String getBacaan() {
        return bacaan;
    }

    public String getGambar() {
        return gambar;
    }

    public String getJumlahKata() {
        return jumlahKata;
    }
}
