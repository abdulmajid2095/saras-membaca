package com.skripsi.saras.Model;

import com.google.gson.annotations.SerializedName;

public class DataSkor {
    @SerializedName("id")
    private String id;

    @SerializedName("nama")
    private String nama;

    @SerializedName("skor")
    private String skor;

    public DataSkor(String id, String nama, String skor){
        this.id = id;
        this.nama = nama;
        this.skor = skor;
    }


    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setnama(String nama) {
        this.nama = nama;
    }

    public String getnama() {
        return nama;
    }

    public String getskor() {
        return skor;
    }

    public void setskor(String skor) {
        this.skor = skor;
    }
}
