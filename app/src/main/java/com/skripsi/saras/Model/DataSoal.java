package com.skripsi.saras.Model;

import com.google.gson.annotations.SerializedName;

public class DataSoal {

    @SerializedName("idSoal")
    private String idSoal;

    @SerializedName("idKategori")
    private String idKategori;

    @SerializedName("soal")
    private String soal;

    @SerializedName("opsiA")
    private String opsiA;

    @SerializedName("opsiB")
    private String opsiB;

    @SerializedName("opsiC")
    private String opsiC;

    @SerializedName("opsiD")
    private String opsiD;

    @SerializedName("jawaban")
    private String jawaban;

    public DataSoal(String idSoal, String idKategori, String soal, String opsiA, String opsiB, String opsiC, String opsiD, String jawaban){
        this.idSoal = idSoal;
        this.idKategori = idKategori;
        this.soal = soal;
        this.opsiA = opsiA;
        this.opsiB = opsiB;
        this.opsiC = opsiC;
        this.opsiD = opsiD;
        this.jawaban = jawaban;
    }

    public String getIdSoal() {
        return idSoal;
    }

    public String getidKategori() {
        return idKategori;
    }

    public String getsoal() {
        return soal;
    }

    public String getopsiA() {
        return opsiA;
    }

    public String getopsiB() {
        return opsiB;
    }

    public String getopsiC() {
        return opsiC;
    }

    public String getopsiD() {
        return opsiD;
    }

    public String getJawaban() {
        return jawaban;
    }
}
