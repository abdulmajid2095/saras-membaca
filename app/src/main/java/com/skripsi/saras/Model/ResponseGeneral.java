package com.skripsi.saras.Model;

import com.google.gson.annotations.SerializedName;

public class ResponseGeneral {

    @SerializedName("status")
    private String status;

    @SerializedName("message")
    private int message;

    public ResponseGeneral(String status, int message){
        this.status = status;
        this.message = message;
    }

    public String getstatus() {
        return status;
    }

    public void setstatus(String status) {
        this.status = status;
    }

    public int getmessage() {
        return message;
    }

    public void setmessage(int message) {
        this.message = message;
    }
}
