package com.skripsi.saras.Model;

import com.google.gson.annotations.SerializedName;

public class SendIdKategori {
    @SerializedName("idKategori")
    private String idKategori;

    public SendIdKategori(String idKategori){
        this.idKategori = idKategori;
    }


    public void setidKategori(String idKategori) {
        this.idKategori = idKategori;
    }

    public String getidKategori() {
        return idKategori;
    }
}
