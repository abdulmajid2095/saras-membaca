package com.skripsi.saras.Model;

import com.google.gson.annotations.SerializedName;

public class SendImei {
    @SerializedName("imei")
    private String imei;

    public SendImei(String imei){
        this.imei = imei;
    }


    public void setimei(String imei) {
        this.imei = imei;
    }

    public String getimei() {
        return imei;
    }
}
