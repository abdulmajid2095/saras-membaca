package com.skripsi.saras.Model;

import com.google.gson.annotations.SerializedName;

public class SendSkor {
    @SerializedName("imei")
    private String imei;

    @SerializedName("nama")
    private String nama;

    @SerializedName("skor")
    private String skor;

    @SerializedName("kategori")
    private String kategori;

    public SendSkor(String imei, String nama, String skor, String kategori){
        this.imei = imei;
        this.nama = nama;
        this.skor = skor;
        this.kategori = kategori;
    }


    public void setimei(String imei) {
        this.imei = imei;
    }

    public String getimei() {
        return imei;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNama() {
        return nama;
    }

    public void setSkor(String skor) {
        this.skor = skor;
    }

    public String getSkor() {
        return skor;
    }

    public void setKategori(String kategori) {
        this.kategori = kategori;
    }

    public String getKategori() {
        return kategori;
    }
}
