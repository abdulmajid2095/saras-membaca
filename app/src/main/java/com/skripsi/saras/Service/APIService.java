package com.skripsi.saras.Service;

import com.skripsi.saras.Model.DataKategori;
import com.skripsi.saras.Model.DataSkor;
import com.skripsi.saras.Model.DataSoal;
import com.skripsi.saras.Model.ResponseGeneral;
import com.skripsi.saras.Model.SendIdKategori;
import com.skripsi.saras.Model.SendImei;
import com.skripsi.saras.Model.SendSkor;

import java.util.List;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface APIService {

    // @M - Lihat Daftar Skor
    @POST("daftarSkor.php")
    Call<List<DataSkor>> lihatDaftarSkor (@Body SendImei body);

    // @M - Lihat Daftar Kategori
    @POST("semuaKategori.php")
    Call<List<DataKategori>> lihatKategori ();

    // @M - Lihat Daftar Soal
    @POST("daftarSoal.php")
    Call<List<DataSoal>> daftarSoal (@Body SendIdKategori body);

    // @M - Kirim Skor
    @POST("insertSkor.php")
    Call<ResponseGeneral> postSkor (@Body SendSkor body);

}