package com.skripsi.saras;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.ImageView;

import com.skripsi.saras.LocalDatabase.DataUser;
import com.skripsi.saras.Service.MusicService;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Settings extends AppCompatActivity {

    @BindView(R.id.back)
    ImageView back;

    @BindView(R.id.switchTimer)
    androidx.appcompat.widget.SwitchCompat switchTimer;

    @BindView(R.id.switchSound)
    androidx.appcompat.widget.SwitchCompat switchSound;

    DataUser dataUser;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_settings);

        ButterKnife.bind(this);

        dataUser = DataUser.findById(DataUser.class, 1L);
        if(dataUser.showTimer == true)
        {
            switchTimer.setChecked(true);
        }
        else
        {
            switchTimer.setChecked(false);
        }

        if(dataUser.soundOn == true)
        {
            switchSound.setChecked(true);
        }
        else
        {
            switchSound.setChecked(false);
        }

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        switchTimer.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                dataUser.showTimer = b;
                dataUser.save();
            }
        });

        switchSound.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                dataUser.soundOn = b;
                dataUser.save();

                if(b)
                {
                    startService();
                }
                else
                {
                    stopService();
                }
            }
        });
    }

    public void startService()
    {
        startService(new Intent(this, MusicService.class));
    }

    public void stopService()
    {
        stopService(new Intent(this, MusicService.class));
    }

}
