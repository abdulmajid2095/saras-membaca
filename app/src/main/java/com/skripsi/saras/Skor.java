package com.skripsi.saras;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.skripsi.saras.Adapter.AdapterSkor;
import com.skripsi.saras.LocalDatabase.DataUser;
import com.skripsi.saras.Model.DataSkor;
import com.skripsi.saras.Model.SendImei;
import com.skripsi.saras.Service.APIService;
import com.skripsi.saras.Service.ApiClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Skor extends AppCompatActivity {

    @BindView(R.id.rvSkor) RecyclerView rvSkor;
    List<DataSkor> listAllSkor = new ArrayList<>();
    AdapterSkor myAdapter;
    SweetAlertDialog loading;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_skor);

        ButterKnife.bind(this);

        rvSkor.setLayoutManager(new LinearLayoutManager(this));
        myAdapter = new AdapterSkor(this, rvSkor, listAllSkor);

        getDataSkor();

    }

    public void loading() {
        // @M - This is function for showing loading dialog
        loading = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        loading.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        loading.setTitleText("Silakan Tunggu ...");
        loading.setCancelable(false);
        loading.show();
    }

    public void getDataSkor()
    {
        loading();
        listAllSkor.clear();

        DataUser dataUser = DataUser.findById(DataUser.class, 1L);
        SendImei sendImei = new SendImei(""+dataUser.imei);

        APIService service = ApiClient.getClient().create(APIService.class);
        Call<List<DataSkor>> userCall = service.lihatDaftarSkor(sendImei);
        userCall.enqueue(new Callback<List<DataSkor>>() {
            @Override
            public void onResponse(Call<List<DataSkor>> call, Response<List<DataSkor>> response) {
                loading.dismiss();
                if (response.isSuccessful())
                {
                    try {
                        List<DataSkor> myListSkor = null;
                        myListSkor = (List<DataSkor>) response.body();
                        if(myListSkor.size()==0)
                        {
                            Toast.makeText(getApplicationContext(), "Skor Kosong", Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            for (int i = 0; i < myListSkor.size(); i++)
                            {
                                DataSkor dataSkor = new DataSkor(
                                        ""+myListSkor.get(i).getId(),
                                        ""+myListSkor.get(i).getnama(),
                                        ""+myListSkor.get(i).getskor()
                                );
                                listAllSkor.add(dataSkor);
                            }

                            // @M - Set up adapter for gridview and notify when data changed
                            rvSkor.setAdapter(myAdapter);
                            myAdapter.notifyDataSetChanged();
                            myAdapter.setLoaded();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                else
                {
                    koneksiError();
                    JSONObject jObjError = null;
                    try {
                        jObjError = new JSONObject(response.errorBody().string());
                        //JSONObject jsonObj = new JSONObject(""+jObjError.getString("error"));
                        Toast.makeText(getApplicationContext(), jObjError.getString("message"), Toast.LENGTH_LONG).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onFailure(Call<List<DataSkor>> call, Throwable t)
            {
                loading.dismiss();
                Toast.makeText(getApplicationContext(), ""+t.getMessage(), Toast.LENGTH_LONG).show();
                koneksiError();
            }
        });
    }


    public void koneksiError() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_koneksi);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(false);

        Button ya = dialog.findViewById(R.id.ya);
        ya.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                getDataSkor();
            }
        });

        dialog.show();
    }
}
