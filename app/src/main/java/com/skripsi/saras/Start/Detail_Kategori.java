package com.skripsi.saras.Start;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.skripsi.saras.LocalDatabase.DataUser;
import com.skripsi.saras.R;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Detail_Kategori extends AppCompatActivity {

    @BindView(R.id.back) ImageView back;
    @BindView(R.id.selesai) LinearLayout selesai;
    @BindView(R.id.timer) TextView timer;
    @BindView(R.id.judul) TextView textJudul;
    @BindView(R.id.bacaan) TextView textBaca;
    @BindView(R.id.gambar) ImageView gambar;

    CountDownTimer countDownTimer;
    String idKategori, judul, bacaan, getGambar, jumlahKata;

    DataUser dataUser;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_detail__kategori);

        ButterKnife.bind(this);
        dataUser = DataUser.findById(DataUser.class, 1L);

        idKategori = ""+getIntent().getStringExtra("idKategori");
        judul = ""+getIntent().getStringExtra("judul");
        bacaan = ""+getIntent().getStringExtra("bacaan");
        getGambar = ""+getIntent().getStringExtra("gambar");
        jumlahKata = ""+getIntent().getStringExtra("jumlahKata");

        textJudul.setText(""+judul);
        textBaca.setText(""+bacaan);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        selesai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                countDownTimer.cancel();
                Intent intent = new Intent(Detail_Kategori.this, Soal.class);
                intent.putExtra("idKategori",""+idKategori);
                intent.putExtra("judul",""+judul);
                intent.putExtra("bacaan",""+bacaan);
                intent.putExtra("gambar",""+getGambar);
                intent.putExtra("jumlahKata",""+jumlahKata);
                intent.putExtra("timer",""+timer.getText().toString());
                startActivity(intent);
            }
        });

        countDownTimer = new CountDownTimer(1800001, 1000) { // adjust the milli seconds here

            public void onTick(long millisUntilFinished)
            {
                long x = 1800001 - millisUntilFinished;
                timer.setText(""+String.format("%d", TimeUnit.MILLISECONDS.toMinutes( x))+":"+""+String.format("%d", TimeUnit.MILLISECONDS.toSeconds(x) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(x))));
            }

            public void onFinish()
            {

            }
        };

        Picasso.with(this)
                .load("https://tumbas.net/membaca/image/" + getGambar)
                .noPlaceholder()
                .resize(400, 400)
                .centerCrop()
                .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                .into(gambar);

        if(dataUser.showTimer == true)
        {
            timer.setVisibility(View.VISIBLE);
        }
        else
        {
            timer.setVisibility(View.INVISIBLE);
        }

        dialogMulai();

    }


    public void dialogMulai() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_start);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(false);

        Button ya = dialog.findViewById(R.id.ya);
        ya.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                countDownTimer.start();
            }
        });

        dialog.show();
    }

}
