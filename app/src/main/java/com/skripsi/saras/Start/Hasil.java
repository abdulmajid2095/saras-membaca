package com.skripsi.saras.Start;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.skripsi.saras.LocalDatabase.DataUser;
import com.skripsi.saras.MenuUtama;
import com.skripsi.saras.Model.ResponseGeneral;
import com.skripsi.saras.Model.SendSkor;
import com.skripsi.saras.R;
import com.skripsi.saras.Service.APIService;
import com.skripsi.saras.Service.ApiClient;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Hasil extends AppCompatActivity {

    @BindView(R.id.oke) LinearLayout oke;
    @BindView(R.id.textSkor) TextView textSkor;

    String idKategori, hasil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_hasil);

        ButterKnife.bind(this);

        idKategori = ""+getIntent().getStringExtra("idKategori");
        hasil = ""+getIntent().getStringExtra("hasil");

        Log.e("track","idKategori : "+idKategori);
        Log.e("track","hasil : "+hasil);

        textSkor.setText(""+hasil);

        oke.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Hasil.this, MenuUtama.class);
                startActivity(intent);
            }
        });

        sendSkor();
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }



    public void sendSkor()
    {
        DataUser dataUser = DataUser.findById(DataUser.class,1L);
        SendSkor sendSkor = new SendSkor(""+dataUser.imei, ""+dataUser.nama, ""+hasil, ""+idKategori);

        APIService service = ApiClient.getClient().create(APIService.class);
        Call<ResponseGeneral> userCall = service.postSkor(sendSkor);
        userCall.enqueue(new Callback<ResponseGeneral>() {
            @Override
            public void onResponse(Call<ResponseGeneral> call, Response<ResponseGeneral> response) {
                if (response.isSuccessful())
                {
                    
                }
                else {

                }

            }

            @Override
            public void onFailure(Call<ResponseGeneral> call, Throwable t)
            {
                
            }
        });
    }
}
