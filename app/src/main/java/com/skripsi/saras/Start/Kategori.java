package com.skripsi.saras.Start;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.skripsi.saras.Adapter.AdapterKategori;
import com.skripsi.saras.Model.DataKategori;
import com.skripsi.saras.R;
import com.skripsi.saras.Service.APIService;
import com.skripsi.saras.Service.ApiClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Kategori extends AppCompatActivity {

    @BindView(R.id.back)
    ImageView back;

    @BindView(R.id.rvKategori) RecyclerView rvKategori;
    List<DataKategori> listAllKategori = new ArrayList<>();
    AdapterKategori myAdapter;
    SweetAlertDialog loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_kategori);

        ButterKnife.bind(this);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        rvKategori.setLayoutManager(new LinearLayoutManager(this));
        myAdapter = new AdapterKategori(this, rvKategori, listAllKategori);

        getDataKategori();
    }



    public void loading() {
        // @M - This is function for showing loading dialog
        loading = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        loading.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        loading.setTitleText("Silakan Tunggu ...");
        loading.setCancelable(false);
        loading.show();
    }

    public void getDataKategori()
    {
        loading();
        listAllKategori.clear();
        
        APIService service = ApiClient.getClient().create(APIService.class);
        Call<List<DataKategori>> userCall = service.lihatKategori();
        userCall.enqueue(new Callback<List<DataKategori>>() {
            @Override
            public void onResponse(Call<List<DataKategori>> call, Response<List<DataKategori>> response) {
                loading.dismiss();
                if (response.isSuccessful())
                {
                    try {
                        List<DataKategori> myListSkor = null;
                        myListSkor = (List<DataKategori>) response.body();
                        if(myListSkor.size()==0)
                        {
                            Toast.makeText(getApplicationContext(), "Kategori tidak ditemukan", Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            for (int i = 0; i < myListSkor.size(); i++)
                            {
                                DataKategori dataKategori = new DataKategori(
                                        ""+myListSkor.get(i).getidKategori(),
                                        ""+myListSkor.get(i).getjudul(),
                                        ""+myListSkor.get(i).getDeskripsi(),
                                        ""+myListSkor.get(i).getBacaan(),
                                        ""+myListSkor.get(i).getGambar(),
                                        ""+myListSkor.get(i).getJumlahKata()
                                );
                                listAllKategori.add(dataKategori);
                                //Toast.makeText(getApplicationContext(),""+i,Toast.LENGTH_SHORT).show();
                            }

                            // @M - Set up adapter for gridview and notify when data changed
                            rvKategori.setAdapter(myAdapter);
                            myAdapter.notifyDataSetChanged();
                            myAdapter.setLoaded();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                else
                {
                    koneksiError();
                    JSONObject jObjError = null;
                    try {
                        jObjError = new JSONObject(response.errorBody().string());
                        //JSONObject jsonObj = new JSONObject(""+jObjError.getString("error"));
                        Toast.makeText(getApplicationContext(), jObjError.getString("message"), Toast.LENGTH_LONG).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onFailure(Call<List<DataKategori>> call, Throwable t)
            {
                loading.dismiss();
                koneksiError();
                Toast.makeText(getApplicationContext(), ""+t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    public void koneksiError() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_koneksi);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(false);

        Button ya = dialog.findViewById(R.id.ya);
        ya.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                getDataKategori();
            }
        });

        dialog.show();
    }
}
