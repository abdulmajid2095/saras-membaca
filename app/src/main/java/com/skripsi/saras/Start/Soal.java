package com.skripsi.saras.Start;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.skripsi.saras.Model.DataJawaban;
import com.skripsi.saras.Model.DataSoal;
import com.skripsi.saras.Model.DataSoal;
import com.skripsi.saras.Model.SendIdKategori;
import com.skripsi.saras.R;
import com.skripsi.saras.Service.APIService;
import com.skripsi.saras.Service.ApiClient;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataOutput;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Soal extends AppCompatActivity {

    @BindView(R.id.selesai) LinearLayout selesai;
    @BindView(R.id.judul) TextView judul;
    @BindView(R.id.gambar) ImageView gambar;
    @BindView(R.id.layAdd) LinearLayout layAdd;

    List<DataSoal> listAllSoal = new ArrayList<>();
    List<DataJawaban> jawabanSiswa = new ArrayList<>();

    SweetAlertDialog loading;
    String idKategori, getJudul, bacaan, getGambar, timer;
    int jumlahKata=0;

    double totalMenit = 0.0;
    int jawabanBenar = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_soal);

        ButterKnife.bind(this);

        idKategori = ""+getIntent().getStringExtra("idKategori");
        getJudul = ""+getIntent().getStringExtra("judul");
        bacaan = ""+getIntent().getStringExtra("bacaan");
        getGambar = ""+getIntent().getStringExtra("gambar");
        timer = ""+getIntent().getStringExtra("timer");

        Log.e("track","idKategori : "+idKategori);
        Log.e("track","timer : "+timer);

        try {
            jumlahKata = Integer.parseInt(""+getIntent().getStringExtra("jumlahKata"));
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        judul.setText(""+getJudul);
        Picasso.with(this)
                .load("https://tumbas.net/membaca/image/" + getGambar)
                .noPlaceholder()
                .resize(400, 400)
                .centerCrop()
                .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                .into(gambar);

        selesai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cekJawabanBenar();
            }
        });
        getTime();
        getDataSoal();
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Toast.makeText(getApplicationContext(),"Ayo selesaikan Pertanyaannya !",Toast.LENGTH_SHORT).show();
    }

    public void getTime()
    {
        List<String> listText;
        if(!timer.equals(""))
        {
            listText = Arrays.asList(timer.split(":"));
            String sMenit = listText.get(0);
            String sDetik = listText.get(1);

            double timeMenit = 0;
            double timeDetik = 0;

            try {
                timeMenit = Double.parseDouble(sMenit);
                timeDetik = Double.parseDouble(sDetik);
                //Toast.makeText(getApplicationContext(),"menitD : "+timeMenit, Toast.LENGTH_SHORT).show();
                //Toast.makeText(getApplicationContext(),"detikD : "+timeDetik, Toast.LENGTH_SHORT).show();

                timeDetik = timeDetik/60;
                //Toast.makeText(getApplicationContext(),"detikD 2 : "+timeDetik, Toast.LENGTH_SHORT).show();
                String x = new DecimalFormat("##.##").format(timeDetik);
                x = x.replace(",",".");
                //Toast.makeText(getApplicationContext(),"x : "+x, Toast.LENGTH_SHORT).show();

                double getMmenitDariDetik = Double.parseDouble(x);
                //Toast.makeText(getApplicationContext(),"detikMenitD : "+getMmenitDariDetik, Toast.LENGTH_SHORT).show();
                totalMenit = timeMenit + getMmenitDariDetik;
                //Toast.makeText(getApplicationContext(),"total Menit "+totalMenit, Toast.LENGTH_SHORT).show();
            } catch (NumberFormatException e) {
                e.printStackTrace();
                //Toast.makeText(getApplicationContext(),"catch "+e.getMessage(),Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void loading() {
        // @M - This is function for showing loading dialog
        loading = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        loading.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        loading.setTitleText("Silakan Tunggu ...");
        loading.setCancelable(false);
        loading.show();
    }


    public void getDataSoal()
    {
        loading();
        listAllSoal.clear();
        jawabanSiswa.clear();

        SendIdKategori sendIdKategori = new SendIdKategori(""+idKategori);
        
        APIService service = ApiClient.getClient().create(APIService.class);
        Call<List<DataSoal>> userCall = service.daftarSoal(sendIdKategori);
        userCall.enqueue(new Callback<List<DataSoal>>() {
            @Override
            public void onResponse(Call<List<DataSoal>> call, Response<List<DataSoal>> response) {
                loading.dismiss();
                if (response.isSuccessful())
                {
                    try {
                        List<DataSoal> myListKategori = null;
                        myListKategori = (List<DataSoal>) response.body();
                        if(myListKategori.size()==0)
                        {
                            Toast.makeText(getApplicationContext(), "Soal tidak ditemukan dalam kategori ini", Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            for (int i = 0; i < myListKategori.size(); i++)
                            {
                                DataSoal dataSoal = new DataSoal(
                                        ""+myListKategori.get(i).getIdSoal(),
                                        ""+myListKategori.get(i).getidKategori(),
                                        ""+myListKategori.get(i).getsoal(),
                                        ""+myListKategori.get(i).getopsiA(),
                                        ""+myListKategori.get(i).getopsiB(),
                                        ""+myListKategori.get(i).getopsiC(),
                                        ""+myListKategori.get(i).getopsiD(),
                                        ""+myListKategori.get(i).getJawaban()
                                );
                                DataJawaban dataJawaban = new DataJawaban("", 0);
                                listAllSoal.add(dataSoal);
                                jawabanSiswa.add(dataJawaban);
                                setSoal(dataSoal, i);
                            }
                            
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                else
                {
                    koneksiError();
                    JSONObject jObjError = null;
                    try {
                        jObjError = new JSONObject(response.errorBody().string());
                        //JSONObject jsonObj = new JSONObject(""+jObjError.getString("error"));
                        Toast.makeText(getApplicationContext(), jObjError.getString("message"), Toast.LENGTH_LONG).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onFailure(Call<List<DataSoal>> call, Throwable t)
            {
                loading.dismiss();
                Toast.makeText(getApplicationContext(), ""+t.getMessage(), Toast.LENGTH_LONG).show();
                koneksiError();
            }
        });
    }



    public void setSoal(DataSoal dataSoal, int position)
    {
        LayoutInflater inflater = LayoutInflater.from(this);
        final View view;
        final LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        view = inflater.inflate(R.layout.model_soal,null);

        TextView nomor = view.findViewById(R.id.nomor);
        TextView soal = view.findViewById(R.id.soal);
        TextView opsiA = view.findViewById(R.id.opsiA);
        TextView opsiB = view.findViewById(R.id.opsiB);
        TextView opsiC = view.findViewById(R.id.opsiC);
        TextView opsiD = view.findViewById(R.id.opsiD);
        RadioButton radioA = view.findViewById(R.id.radioA);
        RadioButton radioB = view.findViewById(R.id.radioB);
        RadioButton radioC = view.findViewById(R.id.radioC);
        RadioButton radioD = view.findViewById(R.id.radioD);

        int numb = position+1;

        nomor.setText(""+numb);
        soal.setText(""+dataSoal.getsoal());
        opsiA.setText(""+dataSoal.getopsiA());
        opsiB.setText(""+dataSoal.getopsiB());
        opsiC.setText(""+dataSoal.getopsiC());
        opsiD.setText(""+dataSoal.getopsiD());

        radioA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                radioA.setChecked(true);
                radioB.setChecked(false);
                radioC.setChecked(false);
                radioD.setChecked(false);
                jawabanSiswa.get(position).setJawaban("a");
                jawabanSiswa.get(position).setDijawab(1);
            }
        });

        radioB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                radioA.setChecked(false);
                radioB.setChecked(true);
                radioC.setChecked(false);
                radioD.setChecked(false);
                jawabanSiswa.get(position).setJawaban("b");
                jawabanSiswa.get(position).setDijawab(1);
            }
        });

        radioC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                radioA.setChecked(false);
                radioB.setChecked(false);
                radioC.setChecked(true);
                radioD.setChecked(false);
                jawabanSiswa.get(position).setJawaban("c");
                jawabanSiswa.get(position).setDijawab(1);
            }
        });

        radioD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                radioA.setChecked(false);
                radioB.setChecked(false);
                radioC.setChecked(false);
                radioD.setChecked(true);
                jawabanSiswa.get(position).setJawaban("d");
                jawabanSiswa.get(position).setDijawab(1);
            }
        });

        view.setLayoutParams(params);
        layAdd.addView(view);
    }

    public void cekJawabanBenar()
    {
        int jumlahSoal = listAllSoal.size();
        int jumlahDijawab = 0;
        for(int i=0; i<jumlahSoal; i++)
        {
            jumlahDijawab = jumlahDijawab + jawabanSiswa.get(i).getDijawab();
        }

        if(jumlahDijawab == jumlahSoal)
        {
            jawabanBenar = 0;

            for(int i=0; i<listAllSoal.size(); i++)
            {
                String jawabanku = ""+jawabanSiswa.get(i).getJawaban();
                String kunci = ""+listAllSoal.get(i).getJawaban();
                if(jawabanku.equals(kunci))
                {
                    jawabanBenar = jawabanBenar + 1;
                }
            }

            hitungSkor();

        }
        else
        {
            Toast.makeText(getApplicationContext(), "Ayo jawab semua soal", Toast.LENGTH_SHORT).show();
        }
    }

    public void hitungSkor()
    {
        Double d1 = jumlahKata/totalMenit;
        Double d2 = jawabanBenar/10.0;

        Log.e("track","jumlah kata : "+jumlahKata);
        Log.e("track","total menit : "+totalMenit);
        Log.e("track","jawaban benar : "+jawabanBenar);

        Double hasil = d1*d2;
        String hasilnya = new DecimalFormat("##.##").format(hasil);

        Log.e("track","Hasil 1 : "+hasil);
        Log.e("track","Hasil 2 : "+hasilnya);

        Intent intent = new Intent(Soal.this, Hasil.class);
        intent.putExtra("idKategori",""+idKategori);
        intent.putExtra("hasil",""+hasilnya);
        startActivity(intent);
    }


    public void koneksiError() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_koneksi);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(false);

        Button ya = dialog.findViewById(R.id.ya);
        ya.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                getDataSoal();
            }
        });

        dialog.show();
    }
}
